#CCP1 Reports

>Monitoring the Murchison Widefield Array Metadata for Antenna Performance and Health  

>Curtin University - Capstone Computing Project One  
Group 9.2 - 2017

#About
This repository is a collection of team documents that don't belong on the other repos for this project. Things such as team, client and supervisor meeting minuts, research reports, the task allocation document, and the SRS can all be found here within their respective directory.
