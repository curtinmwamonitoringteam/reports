# Research Sites

1.  <https://webdesign.tutsplus.com/articles/how-to-architect-a-better-site-map--webdesign-14180>
2.  <https://www.protofuse.com/blog/details/how-to-design-structure-website-with-sitemapping/>
3.  <http://www.dummies.com/web-design-development/site-development/web-design-how-to-create-a-sitemap/>
4.  <https://blog.kissmetrics.com/build-a-sitemap/>

* * *

# Tools

Slickplan (Sitemap and others) <https://slickplan.com/pricing-signup>

-   30 free trial with no card, free account with basic features after 30 days
-   Collaborative editing

Visio <https://products.office.com/en-au/visio/flowchart-software?tab=tabs-1>

-   On all university windows machines
-   Can view diagrams without the software

* * *

# Overview

1.  Prepare for activity of site mapping

    -   Website's objectives?
    -   Who are the users?
    -   What kind of information do they need?

2.  Brainstorm the types of content

    -   What is it we are presenting?
    -   How much information is to be presented / needed?
    -   What helpful resources can we provide?  

3.  Define primary navigation

    -   Be concise
    -   Use common language
    -   Communicate top-down hierarchy
    -   Allow the site to grow  

4.  Flesh out second & third level structure and content
5.  Don't forget about utility pages

    -   Privacy policies, disclaimers, legal information  

6.  Create notes and high-level specifications for each page
7.  Designate the type of design template
8.  Iterate. Iterate. Iterate.

Reference: <https://www.protofuse.com/blog/details/how-to-design-structure-website-with-sitemapping/>

* * *

# Site Map

## Prepare for activity of site mapping

### Website's objectives

1.  Display health and status of dipoles.
2.  

### Users

Anywhere between 1 and a couple of scientists with familiarity and a scientific understanding of the content. Chance of it being briefly used by a new users for a short period of time (?).

### What kind of information do they need?

1.  Status of dipoles.
2.  Historical dipole status.
3.  Dipole locations.

## Brainstorm types of content

### What is it we are presenting?

1.  Map showing dipole tile locations and status.
2.  Table listing malfunctioning dipoles. 
3.  

### How much information is to be presented / needed?

Tile Map:

1.  locations
2.  Health info:
    -   Green = working
    -   Yellow = 
    -   Red = 
    -   Any others?

Malfunctioning Dipole Table:

1.  Tile no.
2.  Dipole no.
3.  

### What helpful resources can we provide?

1.  Help page giving instructions on how to use.
2.  Tooltips on tables / graphs describing what information is being presented, use, etc.

## Define primary navigation

Refer to reference_material/site-map-example-protofuse.png

## Second & third level structure and content

Refer to reference_material/site-map-example-protofuse.png

## Utility pages

Are they needed?

## Notes and high-level specifications for each pages

Number each page in visio or other 'mind-map' software. Write notes for each page.

Refer to reference_material/sitemap-page-specifications-notes.png
