"""
USAGE:  $ python
        >>> alldata, dstate = dptest.loadfile(sample.pickle)

This will load a single pickled dipole health data set. The 'dstate' is an instance of the
DipoleState class defined below, representing which dipoles on which tiles have been flagged
by a human as 'bad' at the time of that test. Ignore this for now, because you don't (yet)
have the code to map between the tile ID (in the dstate object) and the receiver/slot IDs in
the 'alldata' object.

The 'alldata' object is a complicated structure. It is a Python list with 20 elements, corresponding
to the 20 observations in the short dipole test. In order, these are 'All_On', 'All_Off', 'Dipole A',
'Dipole B', ... 'Dipole P', 'All_Off', 'All_On'. The sixteen individual dipoles on the tile are labelled
A through P, and the others are observations where all of the dipoles were turned on, or all turned off
(taken for comparison, to see if the beamformer was pointing).

Each of the 20 elements, one for each observation, is a tuple of (starttime, obsname, data), where
'starttime' is the time in GPS seconds (a 10 digit integer), 'obsname' is a string with the name of
the observation, and 'data' is a numpy array containing the actual power data.

Each of the 'data' elements is a four dimensional array of float32 values. The first axis is
the receiver number (minus 1) (0-15 for the 16 airconditioned boxes in the field), the second axis is the
slot number (minus 1) in that receiver (0-7 for the eight tiles handled by a receiver). These two together define
which physical tile is being measured. The third axis is polarisation (0 for X, 1 for Y). The fourth axis
is frequency channel (0-255, where frequency is 1.28 MHz times the channel number).

To convert the power levels in the data structure into decibels for the plots, we plot
10 * numpy.log10(data + 1).


"""

import cPickle
import importlib
import numpy as np


class Dipole(object):
    """Class to represent a single dipole"""

    def __init__(self, code=None, tileid=None, dipoleid=None, pol='X', comment=''):
        """Here, tileid is a positive integer, dipoleid is EITHER a number from 1-16 OR a letter from A-P,
           and pol is either 'X' or 'Y'.
    
           If 'code' is given, it should be a string encoding all of tileid, dipoleid, and pol, and if valid, it
           overrides all the other parameters. Typical codes are '34DX', '3PY', etc - a one to three digit tile ID,
           followed by a dipole ID as a letter from A to P, followed by either X or Y. Whitespace is ignored.
        """
        self.valid = True
        if code is not None:  # Parse the code and ignore the other arguments
            code = "".join(code.split())  # remove any whitespace
            if len(code) >= 3:
                ts = code[:-2]
                if ts.isdigit():
                    tileid = int(ts)
                dipoleid = code[-2:-1]
                pol = code[-1]

        if (type(tileid) == int) and (tileid > 0):
            self.tileid = tileid
        else:
            self.tileid = None
            self.valid = False

        if (type(dipoleid) == int) and (dipoleid > 0) and (dipoleid < 17):
            self.dipoleid = chr(dipoleid + 64)
        elif (type(dipoleid) == str) and (dipoleid.upper() >= 'A') and (dipoleid.upper() <= 'P'):
            self.dipoleid = dipoleid.upper()
        else:
            self.dipoleid = None
            self.valid = False

        if (type(pol) == str) and (pol.upper() in ['X', 'Y']):
            self.pol = pol.upper()
        else:
            self.pol = None
            self.valid = False

        self.comment = comment

    def __repr__(self):
        if self.valid:
            return "<Dipole(tileid=%s, dipoleid='%s', pol='%s', comment='%s')>" % (
            self.tileid, self.dipoleid, self.pol, self.comment)
        else:
            return "<Invalid Dipole(tileid=%s, dipoleid='%s', pol='%s', comment='%s')>" % (
            self.tileid, self.dipoleid, self.pol, self.comment)

    def __str__(self):
        if self.valid:
            return "T%s %s-%s" % (self.tileid, self.dipoleid, self.pol)
        else:
            return "*T%s %s-%s*" % (self.tileid, self.dipoleid, self.pol)


class DipoleState(object):
    def __init__(self, name=None, reftime=None, db=None):
        self.dipoles = {}  # Dictionary, with tileid as the key
        self.reftime = reftime
        if not name:
            self.name = 'default'
        else:
            self.name = name.lower()

    def __repr__(self):
        return "<DipoleState(name='%s', reftime=%d)>" % (self.name, self.reftime)

    def __str__(self):
        res = "Set '%s' at reftime=%d:\n" % (self.name, self.reftime)
        tlist = self.dipoles.keys()
        tlist.sort()
        for t in tlist:
            dplist = [str(d) for d in self.dipoles[t]]
            res += "  %s\n" % ', '.join(dplist)
        return res

    def gettile(self, tileid):
        """Return the X and Y exclusion lists for the specified tile ID, as sorted lists of numeric dipole ID's,
           to match the format in the database.
        """
        xlist = []
        ylist = []
        for d in self.dipoles[tileid]:
            if d.pol == 'X':
                xlist.append(ord(d.dipoleid) - 64)
            else:
                ylist.append(ord(d.dipoleid) - 64)
        xlist.sort()
        ylist.sort()
        return xlist, ylist

    def save(self, begintime=None, db=None):
        """Save Dipole State object to database.
        """
        pass


def find_global(module_name, class_name):
    if module_name == 'obssched.dipole' and class_name == 'DipoleState':
        return DipoleState
    if module_name == 'obssched.dipole' and class_name == 'Dipole':
        return Dipole
    else:
        mod = importlib.import_module('numpy.core.multiarray')
        return getattr(mod, class_name)


def loadFile(fname=''):
    """Takes the name of a pickled dipole health data file (NNNNNNNNNN.pickle), and
       returns the contents as a tuple of (alldata, dstate), where
    """
    f = open(fname, 'rb')
    unpickler = cPickle.Unpickler(f)
    unpickler.find_global = find_global
    alldata, dstate = unpickler.load()

    return alldata, dstate

def load(file):
    all_data, dstate = loadFile(file)
    return all_data


class Observation:
    values = []

    def __init__(self):
        self.name = None
        self.receiver = None
        self.slot = None
        self.polarity = None
        self.values = []

    def getInfo(self):
        print("Name = " + self.name)


class Test:
    observations = []

    def __init__(self):
        self.receiver = None
        self.slot = None
        self.polarity = None
        self.observations = []

    def getInfo(self):
        print("Receiver " + self.receiver)
        print("Slot " + self.slot)
        print("Polarity " + self.polarity)


def make(data):
    test_list = []

    # create the list
    for receiver in range(16):
        for slot in range(8):
            test = Test()
            test_list.append(test)

            # get associated tile_id
            test.slot = slot
            test.receiver = receiver

            # build observations
            for obs_no in range(20):
                for polarity in range(2):
                    test.polarity = polarity
                    obs = Observation()

                    # get name
                    obs.name = data[obs_no][1]

                    # get correct dipole_id
                    obs.slot = slot
                    obs.receiver = receiver
                    obs.polarity = polarity

                    # get values (one for each frequency channel)
                    values = []
                    for channel in range(256):
                        dipole = data[obs_no][2]
                        values.append(float(dipole[receiver][slot][polarity][channel]))

                    obs.values = values

                    # add the observation to the test
                    test.observations.append(obs)
    return test_list