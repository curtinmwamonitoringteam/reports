# Distinguish Anomaly Types

Tile Graphs: <http://telemetry-static.mwa128t.org/dipoles/>

## Types

### Beam Former

Rainbow effect: 10.4(EDA=99) XY (Scroll to bottom right of page)<http://telemetry-static.mwa128t.org/dipoles/2016-10-01T07.20.39/index.html>
Power while off: 15.2(HexE10=1010) XY <http://telemetry-static.mwa128t.org/dipoles/2016-09-15T07.20.39/index.html>
Clustered lines: Tile 71 XY <http://telemetry-static.mwa128t.org/dipoles/2016-07-31T07.20.31/index.html>

-   Higher standard deviation means it should be flagged

### Flipped Cables

Dipole stronger on X axis than Y axis: 

### Maintenance / Testing

All black tile: 13.7(HexE7=1007) <http://telemetry-static.mwa128t.org/dipoles/2016-09-27T07.20.39/index.html>
Black dipole: Tile 87 Y <http://telemetry-static.mwa128t.org/dipoles/2016-09-15T07.20.39/index.html>

### Bad Electronics

Tile polarization different compared to another tile at that distance: 
Lower power historically: 

-   Means it could be raining
-   Caused by dust on the amplifier when mixed with water, turning it acidic and needs to be fixed.
2.1 (Tile31=31) XY <http://telemetry-static.mwa128t.org/dipoles/2016-08-15T07.20.39/index.html>  

### Digital

RF pollution: Tile 11 XY <http://telemetry-static.mwa128t.org/dipoles/2016-07-16T07.20.39/index.html>

-   Completely ignore this

### Dipole

Deviates above the curve historically: Tile 12 X (Dipole C) <http://telemetry-static.mwa128t.org/dipoles/2016-09-18T07.20.31/index.html>
Deviates under the curve historically: Tile 11.1(Tile61=61) (Dipole O) <http://telemetry-static.mwa128t.org/dipoles/2016-09-18T07.20.31/index.html>

### Other

X and Y different shapes:

Low Powered Tile: 2.3 (Tile33) XY <http://telemetry-static.mwa128t.org/dipoles/2016-08-24T07.20.31/index.html>

Invalid Tile: Tile 27XT <http://telemetry-static.mwa128t.org/dipoles/2016-08-02T07.20.39/index.html>

## Order of operations

### High priority, if error then don't continue

1.  Rainbow effect

-   Power while off, the peak of the curve will be a lot higher than usual. This anomaly follows a very similar curve where the initial increase in power around 60-70Mhz goes a lot further up than a regular tile. From there it stays much higher than normal and eventually drops down at a fast rate. We check for the averages from 100-200Mhz and if they are over 200 then we can flag this as the rainbow effect. This anomaly algorithm will have to be run before the noise removal algorithm.

2.  Clustered lines

-   Off dipole will almost match every other dipole. This is matter of getting the rolling average each 10Mhz (we can skip the initial flat line from 0-60Mhz and 280-328Mhz) for all the dipoles as an overall value. The AllOff dipole will have the rolling average for each 10mhz done seperately. Finally we can compare the overall average to the AllOff average and if the AllOff average matches 90% of the overall average then we can flag this as the clustered lines anomaly.

3.  All black tile

-   Check if the overall average over all dipoles is < 1.

4. Too many black dipoles

- If 4 or more dipoles are considered black then the results we get will not be accurate as the rolling average and standard deviation will be thrown off too much. This will run the black dipole algorithm multiple times and checks if there is 4 or more black dipoles and if so then flag it otherwise continue.

5. Low Powered Tile

- This is when the dipole is showing considerably lower power than usual. It won't be black however its not displaying a normal power either. Get the overall average on all the dipoles and check if it is higher than 1 but lower than 5. If so then check history and repeat process if it is reoccurring then flag it.

6. Invalid Tile

- This is when the values are completely off to what should be normal. First check the values in the 0-60Mhz and if it is higher than 10 then flag it. Get the overall average for the whole range and if it is higher than 300 then flag it.

### Regular priority, continue regardless of error

7.  Power while off

-   Check both the AllOff observations and see if it rises higher than 1.0 and if so flag it. The AllOff values can go up to 0.99 but never over. 

8.  Dipole stronger on x than on y

-   If number of peaks on X are stronger than the peaks on Y. These peaks roughly occur in the 240 to the 280 Mhz range. This is a matter of getting the overall average for each of the dipoles for that range for each of the polarities and comparing them. If the overall average of the X polarity is higher than the Y polarity then flag it as the dipole strong on x than y anomaly.

9.  Black dipole

-   Check if the average of any the dipoles is < 1.

10.  Lower power historically 

-   Below standard deviation since last fixed (last fixed is when a tile that was once flagged no longer has anomalies). Get an overall rolling average and rolling standard deviation for each 10Mhz and ignore 0-60Mhz as well as 240-328Mhz. There are peaks in the 240-280Mhz which are caused by satellites, these can be safely ignored as noise for this anomaly.  Then for each 10Mhz range, we get the lower limit which is going to be the overall average - standard deviation - 1 and rounding down. Then for each dipole we compare its own 10Mhz average to the lower limit. If it falls below that then we get that dipoles history and run the same algortihm again and see if it gets flagged more than twice in a row if so then flag it otherwise its a once off. I minused an extra 1 as having a dipole 1 below the limit is not enough to consider it an anomaly. There may be a larger number to lower the limit by when the analyzing task is complete and we can see how many false positives occur. For now we want to keep it strict rather than relaxed as false positives are easier to deal with.

11. Higher power historically

- Above standard deviation since last fixed (last fixed is when a tile that was once flagged no longer has anomalies). Same as lower power historically except now we are adding the standard deviation + 1 to the average and comparing if the dipole is above it.

12.  RF pollution

-   High power (purple) in the 0-60Hz range and while off. If the power in off and 0.60Mhz is >= 0.98 then flag it.

13. X and Y different shapes

-   Get the overall rolling average and standard deviation every 10Mhz from the range 60-240Mhz. Compare the polarities with each other and make sure they are within 2 standard deviations of each other.

### Do last

14. Tile polarization different compared to another tile at that distance

-   Function of a line, compare to other tiles of same distance that havent been flagged. 
-   Do this at the end of searching for other anomalies. 
