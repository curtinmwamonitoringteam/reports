%-------------------------------------------------------------------------------
% NAME:	    Task_1.6.1_Research-machine-learning.tex
% AUTHOR:   Ryan Scott Day
% LAST MOD:	31/05/2017
% PURPOSE:	CCP1 Research Reports
% REQUIRES:	None
%-------------------------------------------------------------------------------

% Turn off some annoying warnings...
\RequirePackage{silence}
\WarningFilter{hyperref}{Difference (2) between bookmark levels}
\WarningFilter{scrreprt}{Usage of package}
\WarningFilter{Font}{Font shape}
\WarningFilter{Fancyhdr}{\headheight is too small}

%-------------------------------------------------------------------------------
% PACKAGE DECLARATIONS

\documentclass[%
	paper=a4,
    pagesize=auto,
    11pt,
    %twoside=true,
    parskip=half,
    bibliography=totoc
]{report}%

\usepackage[T1]{fontenc}
\usepackage[textheight=23cm, includehead, includefoot, heightrounded]{geometry}
%\usepackage{subfiles}
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{pdflscape}
\usepackage{chngcntr}
%\usepackage[document]{ragged2e} % Changes the spacing of all environments
\usepackage{tcolorbox}
%\usepackage{comment} % Enables the use of multi-line comments (\ifx \fi)
%\usepackage{fullpage} % Changes the margin
\usepackage{lastpage}
\usepackage[english]{babel}
\usepackage{csquotes}
\usepackage[section]{placeins}
\usepackage[backend=biber,style=ieee]{biblatex}
\addbibresource{references.bib}
\usepackage{cleveref}
\usepackage{hyperref}
\hypersetup{
    pdftitle={Capstone Computing Project 1 Research Report}, % title
    pdfauthor={Ryan Scott Day},                     % author
    pdfsubject={TeX and LaTeX},                     % subject of the document
    pdfkeywords={TeX, LaTeX, graphics, images},     % list of keywords
    colorlinks=true,                                % false: boxed links; true: colored links
    linkcolor=blue,                                 % color of internal links
    citecolor=black,                                % color of links to bibliography
    filecolor=black,                                % color of file links
    urlcolor=black,                                 % color of external links
    linktoc=page                                    % only page is linked
}

\pagestyle{fancy}
\frenchspacing 
% http://www.tex.ac.uk/FAQ-floats.html
\renewcommand{\topfraction}{.85}
\renewcommand{\bottomfraction}{.7}
\renewcommand{\textfraction}{.15}
\renewcommand{\floatpagefraction}{.66}
\renewcommand{\dbltopfraction}{.66}
\renewcommand{\dblfloatpagefraction}{.66}
\setcounter{topnumber}{9}
\setcounter{bottomnumber}{9}
\setcounter{totalnumber}{20}
\setcounter{dbltopnumber}{9}

\graphicspath
{ 
	{resources/}
}

%-------------------------------------------------------------------------------
% MISC HELPER FUNCTIONS

% Add line break after paragraph title
\newcommand{\paragraphwb}[1]{\paragraph{#1}\mbox{}\\}
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}

%\setlist{nolistsep}

% Title stuff
\def\name{Ryan Scott Day}
\def\studentid{17160182}
\def\tasknumber{1.6.1}
\def\taskdescription{Research machine learning.}

\renewcommand\thesection{\arabic{section}}

\renewcommand\labelitemi{-}

%-------------------------------------------------------------------------------
% HEADER AND FOOTER

\fancypagestyle{plain}
{%
	\renewcommand{\headrulewidth}{0pt}%
	\renewcommand{\footrulewidth}{0.5pt}
	\fancyhf{}%
	\fancyhead[L]{\emph{\headnotesize Research Report: Task \tasknumber}}
	\fancyfoot[R]{\emph{\footnotesize Page \thepage\ of \pageref{LastPage}}}%
	\fancyfoot[C]{\emph{\footnotesize \name}\\ \emph{\footnotesize \studentid}}%
}

%-------------------------------------------------------------------------------
\begin{document}
%-------------------------------------------------------------------------------
% TITLE PAGE

\input{../title}

%-------------------------------------------------------------------------------
% REPORT CORE

\section{Introduction}

This documents purpose is to outline the research done for the topic: machine learning.

\section{Requirements}

The main use for machine learning in this project is for classifying anomalous readings using the provided power spectra data. When researching machine learning my aim is learn as much as I can about the subject in general, and if I find anything that could help with classifying anomalies, take a note of it.

\section{Research}

The large majority of research I did into machine learning was through the University of California at Berkeley, Concise Machine Learning course lecture notes, published by the unit coordinator Jonathan Richard Shewchuk on May 1, 2017. And through a paper called “A few useful things to know about machine learning”\cite{thingstoknow}. \\

\subsection{Components of a Machine Learning Algorithm}

Any machine learning algorithm consists of a combination of three components\cite{thingstoknow}:

\begin{description}[style=nextline]
	\item[Representation]
		A classifier must be represented in some formal language that the computer can handle. Conversely, choosing a representation for a learner is tantamount to choosing the set of classifiers that it can possibly learn. This set is called the hypothesis space of the learner. If a classifier is not in the hypothesis space, it cannot be learned. A related question, is how to represent the input, i.e., what features to use.
		
		\begin{itemize}
			\item Instances
			\begin{itemize}
				\item K-nearest neighbor
				\item Support vector machines
			\end{itemize}
			\item Hyperplanes
			\begin{itemize}
				\item Naive Bayes
				\item Logistic regression
			\end{itemize}
			\item Decision trees
			\item Sets of rules
			\begin{itemize}
				\item Propositional rules
				\item Logic programs
			\end{itemize}
			\item Neural networks
			\item Graphical models
			\begin{itemize}
				\item Bayesian networks
				\item Conditional random fields
			\end{itemize}
		\end{itemize}
	\item[Evaluation]
		An evaluation function (also called \textit{objective function} or \textit{scoring function}) is needed to distinguish good classifiers from bad ones. The evaluation function used internally by the algorithm may differ from the external one that we want the classifier to optimize, for ease of optimization and due to several issues.
		
		\begin{itemize}
			\item Accuracy / Error rate
			\item Precision and recall
			\item Squared error
			\item Likelihood
			\item Posterior probability
			\item Information gain
			\item K-L divergence
			\item Cost/Utility
			\item Margin
		\end{itemize}
	\item[Optimization] 
		Finally, we need a method to search among the classifiers in the language for the highest-scoring one. The choice of optimization technique is key to the efficiency of the learner, and also helps determine the classifier produced if the evaluation function has more than one optimum. It is common for new learners to start out using off-the-shelf optimizers, which are later replaced by custom-designed ones.
		
		\begin{itemize}
			\item Combinatorial optimization
			\begin{itemize}
				\item Greedy search
				\item Beam search
				\item Branch-and-bound
			\end{itemize}
			\item Continuous optimization
			\begin{itemize}
				\item Unconstrained
				\begin{itemize}
					\item Gradient descent
					\item Conjugate gradient
					\item Quasi-Newton methods
				\end{itemize}
				\item Constrained
				\begin{itemize}
					\item Linear programming
					\item Quadratic programming
				\end{itemize}
			\end{itemize}
		\end{itemize}
\end{description}

%-------------------------------------------------------------------------------
% REFERENCES

\nocite{*}
\printbibliography[title={References}]

%-------------------------------------------------------------------------------
\end{document}
%-------------------------------------------------------------------------------
