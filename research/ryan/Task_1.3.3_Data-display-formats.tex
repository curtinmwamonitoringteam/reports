%-------------------------------------------------------------------------------
% NAME:	    Task_1.3.3_Data-display-formats.tex
% AUTHOR:   Ryan Scott Day
% LAST MOD:	02/06/2017
% PURPOSE:	CCP1 Research Reports
% REQUIRES:	None
%-------------------------------------------------------------------------------

% Turn off some annoying warnings...
\RequirePackage{silence}
\WarningFilter{hyperref}{Difference (2) between bookmark levels}
\WarningFilter{scrreprt}{Usage of package}
\WarningFilter{Font}{Font shape}
\WarningFilter{Fancyhdr}{\headheight is too small}

%-------------------------------------------------------------------------------
% PACKAGE DECLARATIONS

\documentclass[%
	paper=a4,
    pagesize=auto,
    11pt,
    %twoside=true,
    parskip=half,
    bibliography=totoc
]{report}%

\usepackage[T1]{fontenc}
\usepackage[textheight=23cm, includehead, includefoot, heightrounded]{geometry}
%\usepackage{subfiles}
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{pdflscape}
\usepackage{chngcntr}
%\usepackage[document]{ragged2e} % Changes the spacing of all environments
\usepackage{tcolorbox}
%\usepackage{comment} % Enables the use of multi-line comments (\ifx \fi)
%\usepackage{fullpage} % Changes the margin
\usepackage{lastpage}
\usepackage[english]{babel}
\usepackage{csquotes}
\usepackage[section]{placeins}
%\usepackage[backend=biber,style=ieee]{biblatex}
%\addbibresource{bib/references.bib}
\usepackage{cleveref}
\usepackage{hyperref}
\hypersetup{
    pdftitle={Capstone Computing Project 1 Research Report}, % title
    pdfauthor={Ryan Scott Day},                     % author
    pdfsubject={TeX and LaTeX},                     % subject of the document
    pdfkeywords={TeX, LaTeX, graphics, images},     % list of keywords
    colorlinks=true,                                % false: boxed links; true: colored links
    linkcolor=blue,                                 % color of internal links
    citecolor=black,                                % color of links to bibliography
    filecolor=black,                                % color of file links
    urlcolor=black,                                 % color of external links
    linktoc=page                                    % only page is linked
}

\pagestyle{fancy}
\frenchspacing 
% http://www.tex.ac.uk/FAQ-floats.html
\renewcommand{\topfraction}{.85}
\renewcommand{\bottomfraction}{.7}
\renewcommand{\textfraction}{.15}
\renewcommand{\floatpagefraction}{.66}
\renewcommand{\dbltopfraction}{.66}
\renewcommand{\dblfloatpagefraction}{.66}
\setcounter{topnumber}{9}
\setcounter{bottomnumber}{9}
\setcounter{totalnumber}{20}
\setcounter{dbltopnumber}{9}

\graphicspath
{ 
	{resources/}
}

%-------------------------------------------------------------------------------
% MISC HELPER FUNCTIONS

% Add line break after paragraph title
\newcommand{\paragraphwb}[1]{\paragraph{#1}\mbox{}\\}
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}

%\setlist{nolistsep}

% Title stuff
\def\name{Ryan Scott Day}
\def\studentid{17160182}
\def\tasknumber{1.3.3}
\def\taskdescription{Research different data display formats (e.g. The benefits of one type of graph over another).}

\renewcommand\thesection{\arabic{section}}

\renewcommand\labelitemi{-}

%-------------------------------------------------------------------------------
% HEADER AND FOOTER

\fancypagestyle{plain}
{%
	\renewcommand{\headrulewidth}{0pt}%
	\renewcommand{\footrulewidth}{0.5pt}
	\fancyhf{}%
	\fancyhead[L]{\emph{\headnotesize Research Report: Task \tasknumber}}
	\fancyfoot[R]{\emph{\footnotesize Page \thepage\ of \pageref{LastPage}}}%
	\fancyfoot[C]{\emph{\footnotesize \name}\\ \emph{\footnotesize \studentid}}%
}

%-------------------------------------------------------------------------------
\begin{document}
%-------------------------------------------------------------------------------
% TITLE PAGE

\input{../title}

%-------------------------------------------------------------------------------
% REPORT CORE

\section{Introduction}

This document's purpose is to outline the research that has been made when comparing between different data display formats (the benefits of one type of graph over another).

\section{Options}

\href{http://www.qihub.scot.nhs.uk/media/530244/data%20presentation%20types.pdf}{Data Representation Types}

\begin{description}[style=nextline]
	\item[Table] 
		A table shows the raw data presented  in rows and columns. \\
		
		Shoud be used for small datasets for comparison, e.g. number of discharges in a month by a consultant. \\
		
    	\textbf{Pro:}
    	\begin{enumerate}[nolistsep]
        	\item Shows all data
			\item Precise
        \end{enumerate}
    	\textbf{Con}:
    	\begin{enumerate}[nolistsep]
        	\item Can be hard to interpret or see patterns
        \end{enumerate}
	\item[Pie Chart] 
		A pie chart shows data as a percentage of the whole. \\
		
		Should be used for 3-7 categories ony, e.g. patients on the caseload by diagnosis. \\
		
    	\textbf{Pro:}
    	\begin{enumerate}[nolistsep]
        	\item Simple and quick to show proportions
        \end{enumerate}
    	\textbf{Con}:
    	\begin{enumerate}[nolistsep]
        	\item The total represented by the chart is unknown
        \end{enumerate}
	\item[Bar Chart] 
		A bar chart shows data in seperate columns. \\
		
		Should be used for comparison between discrete categories, e.g. patients on the caseload by town. \\
		
    	\textbf{Pro:}
    	\begin{enumerate}[nolistsep]
        	\item Shows scale of the categories
        \end{enumerate}
    	\textbf{Con}:
    	\begin{enumerate}[nolistsep]
        	\item Reordering the bars can change interpretation
        \end{enumerate}
	\item[Histogram] 
		A histogram shows continuous data with no gaps between the columns. \\
		
		Should be used for any data where there are no gaps between the categories, e.g. distance of patients from the community team base. \\
		
    	\textbf{Pro:}
    	\begin{enumerate}[nolistsep]
        	\item Shows continuity of data categories
        \end{enumerate}
    	\textbf{Con}:
    	\begin{enumerate}[nolistsep]
        	\item Data is groups so cannot see individual data
        \end{enumerate}		
	\item[Box and Whisker] 
		Box and Whisker plots show the median and the range of a group of data. \\
		
		Should be used for grouped data that has a spread, e.g. total number of appointments for those discharged in a month. \\
		
    	\textbf{Pro:}
    	\begin{enumerate}[nolistsep]
        	\item Shows average and spread in one picture
        \end{enumerate}
    	\textbf{Con}:
    	\begin{enumerate}[nolistsep]
        	\item Could be difficult to explain to others
        \end{enumerate}
	\item[Scatter Plot] 
		A scatter plot displays all data as single points. \\
		
		Should be used for two sets of numerical data, e.g. total number of appointments in care pathway and change in CORE net score. \\
		
    	\textbf{Pro:}
    	\begin{enumerate}[nolistsep]
        	\item Shows all data points, including outiers
			\item Can highlight correlation
        \end{enumerate}
    	\textbf{Con}:
    	\begin{enumerate}[nolistsep]
        	\item Interpretation can be subjective
			\item Correlation does not mean causation
        \end{enumerate}
	\pagebreak
	\item[Line Chart] 
		A line chart plots data in order and joins them with a line. \\
		
		Should be used for data over time, e.e. number of follow ups per patient, in order the patients were discharged. \\
		
    	\textbf{Pro:}
    	\begin{enumerate}[nolistsep]
        	\item Shows all data points
			\item Is simple to look at
			\item Can show multiple data set
        \end{enumerate}
    	\textbf{Con}:
    	\begin{enumerate}[nolistsep]
        	\item Data must have an order
        \end{enumerate}
	\item[Run Chart] 
		A run chart is a line chart plus the median for the data. \\
		
		Should be used for data over time to look for any changes, e.e. number of new referrals each week. \\
		
    	\textbf{Pro:}
    	\begin{enumerate}[nolistsep]
        	\item Easy to interpret, with 4 easy rules to look for
        \end{enumerate}
    	\textbf{Con}:
    	\begin{enumerate}[nolistsep]
        	\item Cannot be used for unordered categories
        \end{enumerate}
\end{description}

%-------------------------------------------------------------------------------
\end{document}
%-------------------------------------------------------------------------------
