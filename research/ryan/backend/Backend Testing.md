# Backend Testing

The purpose of this research is to test that the webbackend repo works on different machine versions (listed below). The client specified that our code will be deployed on a Ubuntu machine using version 16.04. However, I will be testing on all recently available Ubuntu versions to give the client confidence in changing the servers environment.

Note that [X] means it has been tested and working, [ ] is naturally the opposite.

# Deploying

## webfrontend

Tested and working on Chrome and Firefox. Definitely does not work on IE.

## webbackend

If an error is thrown due to APP_SETTINGS not being found then that means that the global variable APP_SETTINGS hasn't been defined. Within src/app/config.py place the following code above: class Config(object):

"""
from os import environ

if "APP_SETTINGS" not in os.environ:
    os.environ["APP_SETTINGS"] = "prod"
"""

and within src/database/schema_setup.py and src/database/seed_db.py place the following code above the line containing: from app.config import app_config

"""
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(**file**))))
"""

## Requirements

Backend Requirements:

-   git (if cloning from bitbucket repo)
-   python 2.7.12 or greater
-   mysql-server 5.7.18
-   libmysqlclient-dev (any)
-   (optional) mysql workbench
-   appdirs==1.4.3
-   click==6.7
-   coverage==4.4.1
-   Flask==0.12.1
-   Flask-Cors==3.0.3
-   itsdangerous==0.24
-   Jinja2==2.9.6
-   MarkupSafe==1.0
-   MySQL-python==1.2.5
-   numpy==1.13.1
-   packaging==16.8
-   pyparsing==2.2.0
-   six==1.10.0
-   SQLAlchemy~=1.1.14
-   Werkzeug==0.12.1

Frontend Requirements: 

-   git (if cloning from bitbucket repo)
-   curl (to grab node and npm)
-   node >= 4.2.6
-   npm >= 4.0.0  

## Instructions

### Backend

1.  sudo apt-get update
2.  sudo apt-get upgrade
3.  sudo apt-get install git
4.  cd Documents/

-   Change to the directory you want to store the repo in.

5.  git clone <https://ScottDay@bitbucket.org/curtinmwamonitoringteam/webbackend.git>

-   Insert your own username to the git repo before the '@'.
-   Give your password.

6.  cd webbackend/
7.  sudo apt-get install python-pip
8.  sudo pip install virtualenv
9.  virtualenv venv
10. source venv/bin/activate
11. sudo apt-get install libmysqlclient-dev
12. sudo apt-get install python-dev
13. sudo apt-get install mysql-server
14. sudo apt-get install mysql-workbench

-   A prompt will show asking for the root password. Give it whatever you want.

15. pip install -r requirements.txt
16. Open up MySQL Workbench

-   Create a new connection (e.g. name = mwamonitor, user = mwauser, ip = localhost)
-   Open up the root connection and under Users and Privileges, add a new user (e.g. user = mwauser, password = mwaicrar, Administrative roles = DBA admin roles) and hit apply.
-   Close the local instance. Type on the terminal without quotes "/etc/init.d/mysql start".
-   Test in Workbench that the new connection works.
-   Once in the new connection, check if the schema_setup.py include creating a schema. If it doesn't then in the new connection hit the button labeled "create a new schema in the connected server" and follow the prompts.

17. python src/database/schema_setup.py
18. python src/database/seed_db.py

To run the tests:

1.  chmod +x run_tests.sh
2.  ./run_tests.sh

-   Make sure that you have a test database setup with the appropriate connections configured. Set the environment variable APP_SETTINGS to 'test' and then execute. Otherwise you can just change APP_SETTINGS to 'prod' or 'dev' in the code.

To run the server:

1.  Make sure you're in the local venv environment (see above for instructions).
2.  chmod +x devenv.sh
3.  ./devenv.sh

### Frontend

1.  sudo apt-get update
2.  sudo apt-get upgrade
3.  sudo apt-get install git
4.  curl -sL <https://deb.nodesource.com/setup_4.x> | sudo -E bash -
5.  sudo apt-get install nodejs

# Issues

## ImportError: cannot import name '\_remove_dead_weakref'

If you're using pythons virtual environment and you upgrade the system version you might get an error 
along the lines of "ImportError: cannot import name '\_remove_dead_weakref'", it means that venv is 
outdated or pointing to an incorrect python version.

This issue can be resolved by deleting the virtual environment, recreating the environment and 
reinstalling the packages.

Steps:

1.  Navigate to the projects root directory.
2.  If you're already in the virtual environment, deactivate it by typing 'deativate' in the terminal.
3.  Delete the virtual environment directory (assuming its 'venv') with: 'rm -rf venv'
4.  Create the virtual environment: 'python -m virtualenv venv'
5.  Activate the virtual environment: 'source venv/bin/activate'
6.  Install the dependencies: 'pip install -r requirements.txt' 

# Versions

## 17.10

### Desktop

-   [x] 64 bit.
-   32 bit is not available.

### Server

-   [x] 64 bit.
-   [x] 32 bit.

## 17.04

### Desktop

-   [x] 64 bit.
-   [x] 32 bit.

### Server

-   [x] 64 bit.
-   [x] 32 bit.

## 16.04.3

### Desktop

-   [x] 64 bit.
-   [x] 32 bit.

### Server

-   [x] 64 bit.
-   [x] 32 bit.

## 14.04.5

### Desktop

-   [ ] 64 bit.
-   [ ] 32 bit.

### Server

-   [ ] 64 bit.
-   [ ] 32 bit.

## 12.04.5

### Desktop

-   [ ] 64 bit.
-   [ ] 32 bit.

### Server

-   [ ] 64 bit.
-   [ ] 32 bit.
