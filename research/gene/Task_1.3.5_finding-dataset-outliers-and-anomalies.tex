%-------------------------------------------------------------------------------
% NAME:	    template.tex
% AUTHOR:   Ryan Scott Day
% LAST MOD:	31/05/2017
% PURPOSE:	CCP1 Research Reports
% REQUIRES:	None
%-------------------------------------------------------------------------------

% Turn off some annoying warnings...
\RequirePackage{silence}
\WarningFilter{hyperref}{Difference (2) between bookmark levels}
\WarningFilter{scrreprt}{Usage of package}
\WarningFilter{Font}{Font shape}
\WarningFilter{Fancyhdr}{\headheight is too small}

%-------------------------------------------------------------------------------
% PACKAGE DECLARATIONS

\documentclass[%
	paper=a4,
    pagesize=auto,
    11pt,
    %twoside=true,
    parskip=half,
    bibliography=totoc
]{report}%

\usepackage[T1]{fontenc}
\usepackage[textheight=23cm, includehead, includefoot, heightrounded]{geometry}
%\usepackage{subfiles}
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{pdflscape}
\usepackage{chngcntr}
%\usepackage[document]{ragged2e} % Changes the spacing of all environments
\usepackage{tcolorbox}
%\usepackage{comment} % Enables the use of multi-line comments (\ifx \fi)
%\usepackage{fullpage} % Changes the margin
\usepackage{lastpage}
\usepackage[english]{babel}
\usepackage{csquotes}
\usepackage[section]{placeins}
%\usepackage[backend=biber,style=ieee]{biblatex}
%\addbibresource{bib/references.bib}
\usepackage{cleveref}
\usepackage{hyperref}
\hypersetup{
    pdftitle={Capstone Computing Project 1 Research Report}, % title
    pdfauthor={Ryan Scott Day},                     % author
    pdfsubject={TeX and LaTeX},                     % subject of the document
    pdfkeywords={TeX, LaTeX, graphics, images},     % list of keywords
    colorlinks=true,                                % false: boxed links; true: colored links
    linkcolor=blue,                                 % color of internal links
    citecolor=black,                                % color of links to bibliography
    filecolor=black,                                % color of file links
    urlcolor=black,                                 % color of external links
    linktoc=page                                    % only page is linked
}

\pagestyle{fancy}
\frenchspacing 
% http://www.tex.ac.uk/FAQ-floats.html
\renewcommand{\topfraction}{.85}
\renewcommand{\bottomfraction}{.7}
\renewcommand{\textfraction}{.15}
\renewcommand{\floatpagefraction}{.66}
\renewcommand{\dbltopfraction}{.66}
\renewcommand{\dblfloatpagefraction}{.66}
\setcounter{topnumber}{9}
\setcounter{bottomnumber}{9}
\setcounter{totalnumber}{20}
\setcounter{dbltopnumber}{9}

\graphicspath
{ 
	{resources/}
}

%-------------------------------------------------------------------------------
% MISC HELPER FUNCTIONS

% Add line break after paragraph title
\newcommand{\paragraphwb}[1]{\paragraph{#1}\mbox{}\\}
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}

%\setlist{nolistsep}

% Title stuff
\def\name{Gene P. Cross}
\def\studentid{17645375}
\def\tasknumber{1.3.5}
\def\taskdescription{Research methods of determining outliers and anomalies of datasets.}
\def\reportdate{13th April 2017}

\renewcommand\thesection{\arabic{section}}

%-------------------------------------------------------------------------------
% HEADER AND FOOTER

\fancypagestyle{plain}
{%
	\renewcommand{\headrulewidth}{0pt}%
	\renewcommand{\footrulewidth}{0.5pt}
	\fancyhf{}%
	\fancyhead[L]{\emph{\headnotesize Research Report: Task \tasknumber}}
	\fancyfoot[R]{\emph{\footnotesize Page \thepage\ of \pageref{LastPage}}}%
	\fancyfoot[C]{\emph{\footnotesize \name}\\ \emph{\footnotesize \studentid}}%
}

%-------------------------------------------------------------------------------
\begin{document}
%-------------------------------------------------------------------------------
% TITLE PAGE

\input{title}

%-------------------------------------------------------------------------------
% REPORT CORE

\section{Research Discussion}
The results of this research task are intended to assist in solving two primary problems that the project needs to address. Firstly, to discover malfunctioning dipoles, and secondly, to discover and categorize anomalies in the data. To discover a malfunctioning dipole we take the readings of a specific test from each dipole on a tile (ignoring the 'all on' and 'all off' results), and for each frequency find the outlier/s. We record the number of outliers found for each dipole and if the number of outliers for a dipole exceeds a specified threshold we flag the dipole as malfunctioning / needs attention. What constitutes an outlier and what the malfunctioning threshold is, should be easily adjustable and fined tuned over time. This method will find a malfunctioning dipole assuming the majority of other dipoles on the tile are producing 'good' readings.\\
\\ 
Discovering and classifying anomalies in the data is a task that will require more time to devise a method, or methods, to do so. Machine learning may be an integral part to the process of solving this problem, (see research task 1.6 for further information). However there are some known algorithms that we could use that already look promising, such as 'local outlier factor' and 'k nearest neighbour'. These may be more useful in finding 'bands' of anomalous data whereas we could use something such as 'median point filtering' to find isolated spikes in the data set. Median filtering is a noise reduction technique that be described as a simple sliding window spatial filter. It works by replacing the value in the centre of the window with the median value of the window. This smoothes the data, but also means that the resulting data set is no longer perfectly accurate to the initial readings, which is not what we want. We can however take the median filtered value for each data point and compare it to the real value, then applying some deviation measure to determine if it is anomalous. It is worth noting that performance issues may begin to arise at this point and the specific implementation is important. Because of this it may be the better choice to use a selection algorithm, or the 'quick median' method as opposed to sorting to find the median value.\\

\clearpage
\section{Methods}
\begin{tabbing}
\textbf{Determining Outliers}\\
	\hspace{0.5cm}1. Sort the data, low to high.\\
	\hspace{0.5cm}2. Find median, Q1 and Q3.\\
	\hspace{0.5cm}3. Find IQR = Q3 - Q1.\\
	\hspace{0.5cm}4. Find inner fences, data points outside this range are minor outliers.\\
	\hspace{1cm}a. Lower inner fence = Q1 - (IQR * 1.5).\\
	\hspace{1cm}b. Upper inner fence = Q3 + (IQR * 1.5).\\
	\hspace{0.5cm}5. Find outer fences, data points outside this range are major outliers.\\
	\hspace{1cm}a. Lower outer fence = Q1 - (IQR * 3).\\
	\hspace{1cm}b. Upper outer fence = Q3 + (IQR * 3).\\
\end{tabbing}
\\ 

\begin{tabbing}
\textbf{Standard Deviation}\\
	\hspace{0.5cm}1. Find the mean.\\
	\hspace{0.5cm}2. For each number, find the square of its distance to the mean.\\
	\hspace{0.5cm}3. Find the mean of the values from step 2.\\
	\hspace{0.5cm}4. Find the square root of step 3.\\
\end{tabbing}
\\

\begin{tabbing}
\textbf{Other statistical measures to consider}\\
	\hspace{0.5cm}- Rolling deviation.\\
	\hspace{0.5cm}- Stationary deviation.\\
	\hspace{0.5cm}- X point moving averages.\\
\end{tabbing}
\\

%-------------------------------------------------------------------------------
\end{document}
%-------------------------------------------------------------------------------
