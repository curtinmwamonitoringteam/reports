from flask import Flask

app = Flask(__name__)

@app.route('/')
def test_func():
    return 'Hello, World! This is a test to show Flask development. Written By: Jad Sapida 17055819'
