|=======================|
|     Installation      |
|=======================|
- Clone the repository.
	- git clone https://jrsapida@bitbucket.org/curtinmwamonitoringteam/reports.git

- Checkout the jad-final-submission branch
	- git checkout jad-final-submission

- Under research/jad is the source code for the application under sample_flask

- Make a virtual environment
	- pip install virtualenv
	- virtualenv "name"
		where name represents the name of the virtual environment.

- Jump into the virtual environment
	- Linux: source "name"/bin/activate
	- Windows: "name"\Scripts\Activate

- Install Flask
	- pip install Flask

|=======================|
|         Usage         |
|=======================|

- Make sure you are in the virtual environment
	- Windows: run startserver.bat
	- Linux: run startserver.sh