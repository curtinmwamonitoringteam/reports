class Animal(object):
    """
    Attributes:
        name: Represents the name of the animal
        movementAppendage: Refers to the appendage used for movement
    """
    def __init__(self, name, movementAppendage):
        self.name = name
        self.movementAppendage = movementAppendage

    def move(self):
        print 'I am moving using my ' + self.movementAppendage

    def animal_type(self):
        """Return the type of animal"""
        pass

class Bird(Animal):

    def __init__(self, name):
        self.name = name
        self.movementAppendage = 'wings'

    def move(self):
        print self.name + ': I am moving using my ' + self.movementAppendage

    def animal_type(self):
        print self.name + " is a bird!"

class Dog(Animal):

    def __init__(self, name):
        self.name = name
        self.movementAppendage = 'four legs'

    def move(self):
        print self.name + ': I am moving using my ' + self.movementAppendage

    def animal_type(self):
        print self.name + " is a dog!"

def main():
    bob = Bird("bob")
    billy = Dog("billy")

    bob.move()
    bob.animal_type()

    billy.move()
    billy.animal_type()

main()
