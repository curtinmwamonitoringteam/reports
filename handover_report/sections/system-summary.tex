%-------------------------------------------------------------------------------
% NAME:	    system-summary.tex
% AUTHOR:   Ryan Scott Day
% LAST MOD:	10/11/2017
% PURPOSE:	CCP1 Handover Report
% REQUIRES:	NONE
%-------------------------------------------------------------------------------
\documentclass[../handover.tex]{subfiles}
%-------------------------------------------------------------------------------
\begin{document}
%-------------------------------------------------------------------------------

\chapter{System Summary}

Project Source Code Repository Location: \newline 
\href{https://bitbucket.org/account/user/curtinmwamonitoringteam/projects/MWAM}{https://bitbucket.org/account/user/curtinmwamonitoringteam/projects/MWAM}

\section{Frontend}

The repository for the frontend is titled ‘WebFrontend’ and can be found as part of the above project. https://bitbucket.org/curtinmwamonitoringteam/webfrontend 

\subsection{Model}
The class definitions for the frontend can be found in the src/app/model directory. These mimic the model of the backend and are fairly self explanatory, the classes do not have any methods. For further information see the backend model description, section 3.2.2.

\subsection{Angular Module and Component Summary}

This web application is divided up into seven Angular modules each of which encapsulate a number of respective components. Each component can be instantiated multiple times on a web page, for example each power spectra graph is an instance of the PowerSpectraCard component but given different data. A component is comprised of a template, which is the HTML associated with component, a list of styles, in this case a single CSS file, and a spec file, which contains the code to test the component. Each component is essentially just a TypeScript class annotated with @Component() to ‘register’ it within the Angular ecosystem.

\subsection{HTTP Interceptor}

The HTTP interceptor is used in place of the http service provided by Angular. This service is to make routing easier by wrapping the default service in order to prepend the ‘host’ address to a given url, meaning when performing routing calls, only the ‘page extension’ (everything after the port number) is required.

\subsection{Repository Layer}

The repository layer on the frontend consists of a number of services used to interact with the backend. These services should be used when data is required from the backend, they will create and send the http requests, and format the data in the response appropriately. The services return Observables, a stream that must be ‘subscribed’ to for updates / getting data. For more information see the Angular docs on http requests, https://angular.io/api/http/Http.

\subsection{Service Layer}

This layer of the frontend is not defined in a single directory, but as part of each module, namely the main component for each page. For example the PowerSpectra component acts as a controller for the power spectra page, getting the required data through a repository layer service, and generating the graphs by instantiating a list of PowerSpectraCard components which handle the drawing.

\subsection{Main Components}

\subsubsection{Home Page}

The home page contains a title but more importantly a table which shows the active subscribers that will be emailed every time a pickle file is uploaded. This table allows users to add or remove subscribers as necessary.

\subsubsection{Power Spectra}

The power spectra page has two main components, the PowerSpectra component, and the PowerSpectraCard component. The first is the highest level and controls the contents of the page, adding and populating an instance of the Ribbon component at the top of page (beneath the header), and creating the graphs via the PowerSpectraCard components. The PowerSpectraCards take a ChartData object, using it to draw the charts. The ChartData object is given a Test object, then makes a request to the backend to get the associated anomalies, and finally processes the data, ready for use by the drawing library Chart.js.

\subsubsection{Health Reports}

The health reports section has been split into two pages, one for the main page and another for the specific tile page. The main page contains a short summary of all tests in the last couple of months while the specific tile page contains more information about the test for a tile at a given time. The specific tile page also does 2 calculations on the percentage difference in functioning and non-functioning dipoles: one from the previous day and the other for the previous week.

\subsubsection{Statistics}

The statistics section has also been split into two pages, one for the main page and another for the specific tile page. The main page contains a table outlining the number of bad dipoles, a table outlining the tiles that have at least 1 flagged dipole, a table that shows the occurrence rate of each anomaly type and a graph showing the number of anomalies per date. The specific tile page contains almost identical elements but filtered to only contain data from the specific tile chosen.

\subsection{Testing}

Testing the frontend is done by executing npm test in the top level directory. This will start the Karma test runner, in turn executing each test in each spec (test) file found. Karma launches a test server and begins to run the tests in a new Chrome window. Clicking the debug button in the top right will show a report for each test after they are run, providing stack info if a test fails. Writing the Jasmine unit tests is similar to how it would be done with most other languages, but for more information see the Jasmine dependency documentation below, section 7.1.

\section{Backend}

The repository for the frontend is titled ‘WebFrontend’ and can be found as part of the above project. \href{https://bitbucket.org/curtinmwamonitoringteam/webbackend}{https://bitbucket.org/curtinmwamonitoringteam/webbackend}

\subsection{Database Model}

Models for the system can be found in the src/database/model.py file. A class represents a table in the database, as does the association object ‘TestObservation’. Each class is essentially just a container, but also has a method to convert the object to a python dictionary. This is used when serializing the object to be put into a http response. There is also a view model version of each class.

\subsection{Database Scripts}

There are two scripts used when setting up a new database for both production and testing purposes. The first script, src/database/schema\_setup.py takes the model discussed above and creates the required tables in the database specified by the DATABASE\_URI in the app config. The second script src/database/seed\_db.py initializes the database by adding the tiles, dipoles and anomaly types. It should be noted that both of these scripts rely on an environment variable, called APP\_SETTINGS, in order to decide which database to act upon and with what settings. This allows a production, development and testing database to exist alongside each other with interference.

\subsection{Exception Handling}

As Flask practices an Inversion of Control pattern all exceptions are caught by the framework. If we want to handle certain exceptions we can register an error handler that will be given control when the error occurs. The error handlers can be found in src/app/exceptions.py.

\subsection{Repository Layer}

The repository layer on the backend is responsible for interacting with database, creating, reading, updating and deleting rows. Each function in the layer first opens a session with the database, then performs the required actions, and finally closes the session. It is important to not leave dangling sessions as that makes it easy for the database thread pool to become exhausted, potentially causing the backend to hang. Note that the object returned from a database query is not one of the models defined in the system, but rather a query object that needs to have it’s data extracted.

\subsection{Service Layer}

This is the layer that does the ‘thinking’, the processing, and creates the http responses to requests after getting and manipulating the data. The functions in this layer are called by the endpoints, which simply direct a request to the correct service. Most of the services are not that interesting, serving basic CRUD requests, but the upload service is different. This service receives the pickle files, calls the parser to take the raw data from the numpy array and turn it into model objects. After this the data is processed by the analyzer which performs the anomaly detection. The results of all of these actions are stored in the database.

\subsection{Analyzer}

The analyzer processes the tests(tiles) and will go through each test, flagging with specific anomaly types depending on what is found and later store them in the database. Each anomaly type is split into its own algorithm and each of them will run. There are high priority anomalies that if flagged, will cause the analyzing for that test to stop and go on to the next test. The rest are normal and will keep going if flagged. 

Translation of anomaly types into something more useful:

\begin{itemize}
    \item Rainbow effect = Tile not pointing
	\item Clustered Lines = Tile not pointing
	\item Power While Off = Tile not pointing
	\item Invalid Tile = Digital Problem
	\item Dipole with zeroes = Database issue
	\item Dead dipole / tile = Flatline
\end{itemize}

And for examples of each one and a simple explanation of each algorithm: Repo

\subsection{Testing}

Tests on the backend can be executed using the provided bash script, run\_tests.sh. This will search for the unit tests and execute them inside of a code coverage analysis tool. The output will show how many tests passed / failed / errored / skipped, how the test failed / errored, how many statements were executed as percentage of the whole, and the file and line numbers of the statements that were not executed. Writing the tests is mostly a standard, straightforward affair but with added test request context needing to be used, to simulate a URI request.

\section{Uploading the pickle file}

Script / program to run after each dipole test, and upload the pickle file to the /upload endpoint.

%-------------------------------------------------------------------------------
\end{document}
%-------------------------------------------------------------------------------
