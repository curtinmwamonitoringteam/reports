..................EsE........F................E...Es.......E.E.............E....s
----------------------------------------------------------------------
Ran 81 tests in 7.159s

FAILED (failures=1, errors=7, skipped=3)

(venv) D:\git\CurtinMWAProject\webbackend\src>coverage report -m
Name                                                    Stmts   Miss  Cover   Missing
-------------------------------------------------------------------------------------
app\__init__.py                                            13      0   100%
app\config.py                                              21      1    95%   4
app\endpoint\__init__.py                                    0      0   100%
app\endpoint\anomaly_endpoint.py                           18      0   100%
app\endpoint\anomaly_type_endpoint.py                      18      1    94%   23
app\endpoint\dipole_endpoint.py                            12      0   100%
app\endpoint\ignore_tile_endpoint.py                       11      0   100%
app\endpoint\observation_endpoint.py                       15      0   100%
app\endpoint\test_endpoint.py                              22      2    91%   26, 28
app\endpoint\tile_endpoint.py                               9      0   100%
app\endpoint\upload_endpoint.py                             5      1    80%   18
database\__init__.py                                        0      0   100%
database\model.py                                          60      5    92%   34, 59, 85, 111, 140
processor\__init__.py                                       0      0   100%
processor\parsing\__init__.py                               0      0   100%
processor\parsing\dptest.py                                78     64    18%   50-80, 83-87, 91-94, 99-104, 107, 110-116, 122-131, 136, 140-146, 153-158
processor\parsing\parser.py                                30     24    20%   19-69
processor\parsing\tilemap.py                               46     40    13%   30-59, 76-92
repository\__init__.py                                      6      0   100%
repository\anomaly_repository.py                           56      1    98%   97
repository\anomaly_type_repository.py                      56     14    75%   72, 79-83, 97, 139-154
repository\dipole_repository.py                            74     10    86%   96-111, 199
repository\ignore_tile_repository.py                       29      0   100%
repository\observation_repository.py                       48      1    98%   97
repository\test_repository.py                              81     24    70%   25, 129, 181-203, 208-226
repository\tile_repository.py                              46     12    74%   72, 79-83, 96-109
service\__init__.py                                         0      0   100%
service\anomaly_service.py                                 58      0   100%
service\anomaly_type_service.py                            57     13    77%   71-76, 135-146
service\dipole_service.py                                  34      0   100%
service\ignore_tile_service.py                             28      0   100%
service\observation_service.py                             48      0   100%
service\test_service.py                                    88     39    56%   21-43, 178-200, 207-228
service\tile_service.py                                    25      0   100%
service\upload_service.py                                  38     26    32%   24-61, 91-105
test\__init__.py                                            0      0   100%
test\processor_tests\__init__.py                            0      0   100%
test\processor_tests\parsing\__init__.py                    0      0   100%
test\processor_tests\parsing\test_parser.py                 5      0   100%
test\processor_tests\test_analyzer.py                      23      0   100%
test\repository_tests\__init__.py                           0      0   100%
test\repository_tests\test_anomaly_repository.py           68     13    81%   89-112, 146
test\repository_tests\test_anomaly_type_repository.py      49      6    88%   74-82
test\repository_tests\test_dipole_repository.py            72      2    97%   147-148
test\repository_tests\test_ignore_tile_repository.py       40      0   100%
test\repository_tests\test_observation_repository.py       52      0   100%
test\repository_tests\test_test_repository.py              65      0   100%
test\repository_tests\test_tile_repository.py              68     21    69%   74-82, 89-103, 133-139
test\service_tests\__init__.py                              0      0   100%
test\service_tests\test_anomaly_service.py                 70      0   100%
test\service_tests\test_anomaly_type_service.py            55      7    87%   50-55, 96-99
test\service_tests\test_dipole_service.py                  31      0   100%
test\service_tests\test_ignore_tile_service.py             34      0   100%
test\service_tests\test_observation_service.py             66      0   100%
test\service_tests\test_test_service.py                    81     10    88%   45-63
test\service_tests\test_tile_service.py                    26      0   100%
test\service_tests\test_upload_service.py                  33     16    52%   38-66
test\setup_test.py                                         26      3    88%   79-80, 84
test\view_model_tests\__init__.py                           0      0   100%
test\view_model_tests\test_anomaly_type_view.py            32     21    34%   20-22, 30, 36-44, 50-64, 70-74
test\view_model_tests\test_anomaly_view.py                 32     21    34%   20-22, 30, 36-44, 50-64, 70-74
test\view_model_tests\test_dipole_view.py                  35     24    31%   20-22, 30, 36-45, 51-67, 73-78
test\view_model_tests\test_ignore_tile_view.py             29     18    38%   20-22, 30, 36-43, 49-61, 67-70
test\view_model_tests\test_observation_view.py             38     27    29%   20-22, 30, 36-46, 52-70, 76-82
test\view_model_tests\test_test_view.py                    35     24    31%   20-22, 30, 36-45, 51-67, 73-78
test\view_model_tests\test_tile_view.py                    29     18    38%   20-22, 30, 36-43, 49-61, 67-70
view_model\__init__.py                                      0      0   100%
view_model\anomaly_type_view.py                            15      4    73%   16, 21-23
view_model\anomaly_view.py                                 15      4    73%   16, 21-23
view_model\dipole_view.py                                  18     10    44%   15-26
view_model\ignore_tile_view.py                             12      6    50%   13-20
view_model\observation_view.py                             21      6    71%   18, 25-29
view_model\test_view.py                                    21      9    57%   13-16, 19, 34-37
view_model\tile_view.py                                    12      6    50%   14-22
-------------------------------------------------------------------------------------
TOTAL                                                    2338    554    76%

